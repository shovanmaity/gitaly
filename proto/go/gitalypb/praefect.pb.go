// Code generated by protoc-gen-go. DO NOT EDIT.
// source: praefect.proto

package gitalypb

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type ListRepositoriesRequest struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ListRepositoriesRequest) Reset()         { *m = ListRepositoriesRequest{} }
func (m *ListRepositoriesRequest) String() string { return proto.CompactTextString(m) }
func (*ListRepositoriesRequest) ProtoMessage()    {}
func (*ListRepositoriesRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_d32bf44842ead735, []int{0}
}

func (m *ListRepositoriesRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ListRepositoriesRequest.Unmarshal(m, b)
}
func (m *ListRepositoriesRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ListRepositoriesRequest.Marshal(b, m, deterministic)
}
func (m *ListRepositoriesRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ListRepositoriesRequest.Merge(m, src)
}
func (m *ListRepositoriesRequest) XXX_Size() int {
	return xxx_messageInfo_ListRepositoriesRequest.Size(m)
}
func (m *ListRepositoriesRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_ListRepositoriesRequest.DiscardUnknown(m)
}

var xxx_messageInfo_ListRepositoriesRequest proto.InternalMessageInfo

type ListRepositoriesResponse struct {
	Primary              *ListRepositoriesResponse_RepositoryDetails   `protobuf:"bytes,1,opt,name=primary,proto3" json:"primary,omitempty"`
	Replicas             []*ListRepositoriesResponse_RepositoryDetails `protobuf:"bytes,2,rep,name=replicas,proto3" json:"replicas,omitempty"`
	XXX_NoUnkeyedLiteral struct{}                                      `json:"-"`
	XXX_unrecognized     []byte                                        `json:"-"`
	XXX_sizecache        int32                                         `json:"-"`
}

func (m *ListRepositoriesResponse) Reset()         { *m = ListRepositoriesResponse{} }
func (m *ListRepositoriesResponse) String() string { return proto.CompactTextString(m) }
func (*ListRepositoriesResponse) ProtoMessage()    {}
func (*ListRepositoriesResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_d32bf44842ead735, []int{1}
}

func (m *ListRepositoriesResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ListRepositoriesResponse.Unmarshal(m, b)
}
func (m *ListRepositoriesResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ListRepositoriesResponse.Marshal(b, m, deterministic)
}
func (m *ListRepositoriesResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ListRepositoriesResponse.Merge(m, src)
}
func (m *ListRepositoriesResponse) XXX_Size() int {
	return xxx_messageInfo_ListRepositoriesResponse.Size(m)
}
func (m *ListRepositoriesResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ListRepositoriesResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ListRepositoriesResponse proto.InternalMessageInfo

func (m *ListRepositoriesResponse) GetPrimary() *ListRepositoriesResponse_RepositoryDetails {
	if m != nil {
		return m.Primary
	}
	return nil
}

func (m *ListRepositoriesResponse) GetReplicas() []*ListRepositoriesResponse_RepositoryDetails {
	if m != nil {
		return m.Replicas
	}
	return nil
}

type ListRepositoriesResponse_RepositoryDetails struct {
	Repository           *Repository `protobuf:"bytes,1,opt,name=repository,proto3" json:"repository,omitempty"`
	Checksum             string      `protobuf:"bytes,2,opt,name=checksum,proto3" json:"checksum,omitempty"`
	XXX_NoUnkeyedLiteral struct{}    `json:"-"`
	XXX_unrecognized     []byte      `json:"-"`
	XXX_sizecache        int32       `json:"-"`
}

func (m *ListRepositoriesResponse_RepositoryDetails) Reset() {
	*m = ListRepositoriesResponse_RepositoryDetails{}
}
func (m *ListRepositoriesResponse_RepositoryDetails) String() string {
	return proto.CompactTextString(m)
}
func (*ListRepositoriesResponse_RepositoryDetails) ProtoMessage() {}
func (*ListRepositoriesResponse_RepositoryDetails) Descriptor() ([]byte, []int) {
	return fileDescriptor_d32bf44842ead735, []int{1, 0}
}

func (m *ListRepositoriesResponse_RepositoryDetails) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ListRepositoriesResponse_RepositoryDetails.Unmarshal(m, b)
}
func (m *ListRepositoriesResponse_RepositoryDetails) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ListRepositoriesResponse_RepositoryDetails.Marshal(b, m, deterministic)
}
func (m *ListRepositoriesResponse_RepositoryDetails) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ListRepositoriesResponse_RepositoryDetails.Merge(m, src)
}
func (m *ListRepositoriesResponse_RepositoryDetails) XXX_Size() int {
	return xxx_messageInfo_ListRepositoriesResponse_RepositoryDetails.Size(m)
}
func (m *ListRepositoriesResponse_RepositoryDetails) XXX_DiscardUnknown() {
	xxx_messageInfo_ListRepositoriesResponse_RepositoryDetails.DiscardUnknown(m)
}

var xxx_messageInfo_ListRepositoriesResponse_RepositoryDetails proto.InternalMessageInfo

func (m *ListRepositoriesResponse_RepositoryDetails) GetRepository() *Repository {
	if m != nil {
		return m.Repository
	}
	return nil
}

func (m *ListRepositoriesResponse_RepositoryDetails) GetChecksum() string {
	if m != nil {
		return m.Checksum
	}
	return ""
}

func init() {
	proto.RegisterType((*ListRepositoriesRequest)(nil), "gitaly.ListRepositoriesRequest")
	proto.RegisterType((*ListRepositoriesResponse)(nil), "gitaly.ListRepositoriesResponse")
	proto.RegisterType((*ListRepositoriesResponse_RepositoryDetails)(nil), "gitaly.ListRepositoriesResponse.RepositoryDetails")
}

func init() { proto.RegisterFile("praefect.proto", fileDescriptor_d32bf44842ead735) }

var fileDescriptor_d32bf44842ead735 = []byte{
	// 277 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x9c, 0x91, 0xb1, 0x4e, 0xc3, 0x30,
	0x10, 0x86, 0x95, 0x80, 0x4a, 0xb8, 0x22, 0x54, 0xbc, 0x10, 0xb2, 0x10, 0x75, 0xca, 0x42, 0x52,
	0x85, 0x37, 0x40, 0x2c, 0x48, 0x15, 0x43, 0xd8, 0xd8, 0x1c, 0x73, 0x4d, 0x2d, 0x92, 0xda, 0xdc,
	0xb9, 0x48, 0x79, 0x12, 0x26, 0x5e, 0x94, 0x09, 0x91, 0xb4, 0x01, 0x51, 0x55, 0x48, 0x6c, 0xbe,
	0xff, 0x7e, 0x7f, 0xbe, 0xdf, 0x07, 0xa7, 0x96, 0x24, 0x2e, 0x50, 0xb9, 0xd4, 0x92, 0x71, 0x46,
	0x8c, 0x2a, 0xed, 0x64, 0xdd, 0x46, 0x27, 0xbc, 0x94, 0x84, 0x4f, 0xbd, 0x3a, 0xbd, 0x80, 0xf3,
	0xb9, 0x66, 0x57, 0xa0, 0x35, 0xac, 0x9d, 0x21, 0x8d, 0x5c, 0xe0, 0xcb, 0x1a, 0xd9, 0x4d, 0xdf,
	0x7d, 0x08, 0x77, 0x7b, 0x6c, 0xcd, 0x8a, 0x51, 0xcc, 0xe1, 0xc8, 0x92, 0x6e, 0x24, 0xb5, 0xa1,
	0x17, 0x7b, 0xc9, 0x38, 0xcf, 0xd3, 0x9e, 0x9f, 0xee, 0xbb, 0x92, 0x0e, 0x62, 0x7b, 0x8b, 0x4e,
	0xea, 0x9a, 0x8b, 0x2d, 0x42, 0xdc, 0x43, 0x40, 0x68, 0x6b, 0xad, 0x24, 0x87, 0x7e, 0x7c, 0xf0,
	0x4f, 0xdc, 0xc0, 0x88, 0x14, 0x9c, 0xed, 0xb4, 0x45, 0x0e, 0x40, 0x83, 0xb8, 0x99, 0x5a, 0x6c,
	0x9f, 0xf9, 0xb6, 0x17, 0x3f, 0x5c, 0x22, 0x82, 0x40, 0x2d, 0x51, 0x3d, 0xf3, 0xba, 0x09, 0xfd,
	0xd8, 0x4b, 0x8e, 0x8b, 0xa1, 0xce, 0x2d, 0x8c, 0xef, 0x56, 0x0b, 0xf3, 0x80, 0xf4, 0xaa, 0x15,
	0x0a, 0x09, 0x93, 0xdf, 0xb3, 0x8a, 0xcb, 0xfd, 0x29, 0xba, 0x3f, 0x8e, 0xe2, 0xbf, 0x62, 0x4e,
	0x83, 0x8f, 0xb7, 0xe4, 0x30, 0xf0, 0x27, 0xde, 0xcc, 0xbb, 0x99, 0x3d, 0x7e, 0xd9, 0x6b, 0x59,
	0xa6, 0xca, 0x34, 0x59, 0x7f, 0xbc, 0x32, 0x54, 0x65, 0x3d, 0x24, 0xeb, 0x56, 0x9a, 0x55, 0x66,
	0x53, 0xdb, 0xb2, 0x1c, 0x75, 0xd2, 0xf5, 0x67, 0x00, 0x00, 0x00, 0xff, 0xff, 0xf7, 0x02, 0xe2,
	0xe6, 0x0d, 0x02, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// InfoServiceClient is the client API for InfoService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type InfoServiceClient interface {
	ListRepositories(ctx context.Context, in *ListRepositoriesRequest, opts ...grpc.CallOption) (InfoService_ListRepositoriesClient, error)
}

type infoServiceClient struct {
	cc *grpc.ClientConn
}

func NewInfoServiceClient(cc *grpc.ClientConn) InfoServiceClient {
	return &infoServiceClient{cc}
}

func (c *infoServiceClient) ListRepositories(ctx context.Context, in *ListRepositoriesRequest, opts ...grpc.CallOption) (InfoService_ListRepositoriesClient, error) {
	stream, err := c.cc.NewStream(ctx, &_InfoService_serviceDesc.Streams[0], "/gitaly.InfoService/ListRepositories", opts...)
	if err != nil {
		return nil, err
	}
	x := &infoServiceListRepositoriesClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type InfoService_ListRepositoriesClient interface {
	Recv() (*ListRepositoriesResponse, error)
	grpc.ClientStream
}

type infoServiceListRepositoriesClient struct {
	grpc.ClientStream
}

func (x *infoServiceListRepositoriesClient) Recv() (*ListRepositoriesResponse, error) {
	m := new(ListRepositoriesResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// InfoServiceServer is the server API for InfoService service.
type InfoServiceServer interface {
	ListRepositories(*ListRepositoriesRequest, InfoService_ListRepositoriesServer) error
}

// UnimplementedInfoServiceServer can be embedded to have forward compatible implementations.
type UnimplementedInfoServiceServer struct {
}

func (*UnimplementedInfoServiceServer) ListRepositories(req *ListRepositoriesRequest, srv InfoService_ListRepositoriesServer) error {
	return status.Errorf(codes.Unimplemented, "method ListRepositories not implemented")
}

func RegisterInfoServiceServer(s *grpc.Server, srv InfoServiceServer) {
	s.RegisterService(&_InfoService_serviceDesc, srv)
}

func _InfoService_ListRepositories_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(ListRepositoriesRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(InfoServiceServer).ListRepositories(m, &infoServiceListRepositoriesServer{stream})
}

type InfoService_ListRepositoriesServer interface {
	Send(*ListRepositoriesResponse) error
	grpc.ServerStream
}

type infoServiceListRepositoriesServer struct {
	grpc.ServerStream
}

func (x *infoServiceListRepositoriesServer) Send(m *ListRepositoriesResponse) error {
	return x.ServerStream.SendMsg(m)
}

var _InfoService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "gitaly.InfoService",
	HandlerType: (*InfoServiceServer)(nil),
	Methods:     []grpc.MethodDesc{},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "ListRepositories",
			Handler:       _InfoService_ListRepositories_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "praefect.proto",
}
